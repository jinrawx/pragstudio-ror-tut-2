require 'rails_helper'

describe "Deleting a review" do
  before do
    @user1 = User.create(user_attributes(name: "john01",
                                        username: "john01",
                                        email: "john01@example.com",
                                        admin: true))

    @user2 = User.create(user_attributes(name: "john02",
                                        username: "john02",
                                        email: "john02@example.com"))
    sign_in(@user1)
  end
  it "destroys the review and shows the movie listing without the review" do
    movie = Movie.create!(movie_attributes(title: "Iron Man"))
    review1 = movie.reviews.new(review_attributes(comment: "superb!"))
    review2 = movie.reviews.new(review_attributes)

    review1.user = @user1
    review1.save

    review2.user = @user2
    review2.save

    visit movie_reviews_url(movie)

    expect(page).to have_text(review1.user.name)
    expect(page).to have_text(review2.user.name)

    click_link "review_#{review1.id}_delete"

    expect(current_path).to eq(movie_reviews_path(movie))

    expect(page).to have_text("Review successfully deleted!")
    expect(page).not_to have_text(review1.comment)
  end
end
