require 'rails_helper'

describe "delete a user" do
  before do
    admin = User.create!(user_attributes(admin: true))
    sign_in(admin)
  end

  it "deletes a user" do
    user = User.create(user_attributes(name: "Jack",
                                       username: "jack",
                                       email: "jack@example.com"))

    visit user_path(user)

    click_link "Delete account"

    expect(current_path).to eq(root_path)

    expect(page).to have_text("Account successfully deleted.")

    expect(User.count).to eq(1)
  end

  # it "automatically signs out that user" do
  #   user = User.create(user_attributes(name: "Jack",
  #                                      username: "jack",
  #                                      email: "jack@example.com"))
  #
  #   visit user_path(user)
  #
  #   click_link 'Delete account'
  #
  #   expect(page).to have_link('Sign In')
  #   expect(page).not_to have_link('Sign Out')
  # end
end
