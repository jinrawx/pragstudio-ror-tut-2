require 'rails_helper'

describe "Viewing the list of users" do
  it "shows the users" do
    user1 = User.create!(user_attributes(name: "John",
                                        email: "john@example.com",
                                        username: "john01"))
    user2 = User.create!(user_attributes(name: "Jimmy",
                                        email: "jimmy@example.com",
                                        username: "jimmy01"))

    sign_in(user1)

    visit users_path

    expect(page).to have_link("John")
    expect(page).to have_text("john@example.com")

    expect(page).to have_link("Jimmy")
    expect(page).to have_text("jimmy@example.com")
  end
end
