require 'rails_helper'

describe "sign out a user" do
  it "signs out a user and redirects to home page" do
    user = User.create!(user_attributes)

    sign_in(user)

    click_link "Sign Out"

    expect(current_path).to eq(root_path)
    expect(page).to have_text("Successfully logged out.")
    expect(page).to have_link("Sign In")
    expect(page).not_to have_link('Sign Out')
  end
end
