require 'rails_helper'

describe "Viewing a user's profile page" do
  it "shows the user's details" do
    user = User.create(user_attributes(name: "John",
                                       email: "john@example.com"))

    sign_in(user)
    
    visit user_path(user)

    expect(page).to have_text(user.name)
    expect(page).to have_text(user.email)
  end
end
